# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Victor Ibragimov <victor.ibragimov@gmail.com>, 2009, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: systemsettings\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-01 00:49+0000\n"
"PO-Revision-Date: 2021-09-09 23:38-0700\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: Tajik Language Support\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Victor Ibragimov"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "victor.ibragimov@gmail.com"

#: app/main.cpp:61 app/SettingsBase.cpp:64
#, kde-format
msgid "Info Center"
msgstr "Маркази иттилоотӣ"

#: app/main.cpp:63
#, kde-format
msgid "Centralized and convenient overview of system information."
msgstr ""

#: app/main.cpp:65 app/main.cpp:76 icons/IconMode.cpp:58
#, kde-format
msgid "(c) 2009, Ben Cooksley"
msgstr "(c) 2009, Бэн Куксли (Ben Cooksley)"

#: app/main.cpp:72 app/SettingsBase.cpp:67 runner/systemsettingsrunner.cpp:188
#: sidebar/package/contents/ui/introPage.qml:63
#, kde-format
msgid "System Settings"
msgstr "Танзимоти низом"

#: app/main.cpp:74
#, kde-format
msgid "Central configuration center by KDE."
msgstr "Маркази идоракунии низоми KDE."

#: app/main.cpp:87 icons/IconMode.cpp:59 sidebar/SidebarMode.cpp:181
#, kde-format
msgid "Ben Cooksley"
msgstr "Бэн Куксли (Ben Cooksley)"

#: app/main.cpp:87
#, kde-format
msgid "Maintainer"
msgstr "Муассис"

#: app/main.cpp:88 icons/IconMode.cpp:60 sidebar/SidebarMode.cpp:182
#, kde-format
msgid "Mathias Soeken"
msgstr "Матиас Сойкен (Mathias Soeken)"

#: app/main.cpp:88 icons/IconMode.cpp:60 sidebar/SidebarMode.cpp:182
#, kde-format
msgid "Developer"
msgstr "Барномасоз"

#: app/main.cpp:89
#, kde-format
msgid "Will Stephenson"
msgstr "Вил Стефенсон (Will Stephenson)"

#: app/main.cpp:89
#, kde-format
msgid "Internal module representation, internal module model"
msgstr "Намояндагии дохилии модул, намунаи дохилии модул"

#: app/main.cpp:97
#, kde-format
msgid "List all possible modules"
msgstr "Рӯйхати ҳамаи модулҳои имконпазир"

#: app/main.cpp:98 app/main.cpp:161
#, kde-format
msgid "Configuration module to open"
msgstr ""

#: app/main.cpp:99 app/main.cpp:162
#, kde-format
msgid "Arguments for the module"
msgstr ""

#: app/main.cpp:107
#, kde-format
msgid "The following modules are available:"
msgstr "Модулҳои зерин дастрасанд:"

#: app/main.cpp:125
#, kde-format
msgid "No description available"
msgstr "Ягон тавсиф нест"

#: app/SettingsBase.cpp:58
#, kde-format
msgctxt "Search through a list of control modules"
msgid "Search"
msgstr "Ҷустуҷӯ"

#: app/SettingsBase.cpp:155
#, fuzzy, kde-format
#| msgid "Icon View"
msgid "Switch to Icon View"
msgstr "Намуди нишонаҳо"

#: app/SettingsBase.cpp:162
#, fuzzy, kde-format
#| msgid "Sidebar View"
msgid "Switch to Sidebar View"
msgstr "Намуди навори ҷонибӣ"

#: app/SettingsBase.cpp:171
#, kde-format
msgid "Report a Bug in the Current Page…"
msgstr ""

#: app/SettingsBase.cpp:198
#, kde-format
msgid "Help"
msgstr "Кумак"

#: app/SettingsBase.cpp:354
#, kde-format
msgid ""
"System Settings was unable to find any views, and hence has nothing to "
"display."
msgstr ""
"Барномаи танзимоти низом ягон намудро ёфт карда натавонист, бинобар ин ягон "
"чиз барои намоиш дастрас нест."

#: app/SettingsBase.cpp:354
#, kde-format
msgid "No views found"
msgstr "Ягон намуд ёфт нашуд"

#: app/SettingsBase.cpp:408
#, kde-format
msgid "About Active View"
msgstr "Дар бораи намуди фаъол"

#: app/SettingsBase.cpp:479
#, kde-format
msgid "About %1"
msgstr "Дар бораи %1"

#. i18n: ectx: label, entry (ActiveView), group (Main)
#: app/systemsettings.kcfg:9
#, kde-format
msgid "Internal name for the view used"
msgstr "Номи дохилӣ барои намуди истифодашаванда"

#. i18n: ectx: ToolBar (mainToolBar)
#: app/systemsettingsui.rc:15
#, kde-format
msgid "About System Settings"
msgstr "Дар бораи танзимоти низом"

#: app/ToolTips/tooltipmanager.cpp:188
#, kde-format
msgid "Contains 1 item"
msgid_plural "Contains %1 items"
msgstr[0] "Дорои 1 мавод мебошад"
msgstr[1] "Дорои %1 мавод мебошад"

#: core/ExternalAppModule.cpp:27
#, kde-format
msgid "%1 is an external application and has been automatically launched"
msgstr "%1 барномаи берунӣ аст ва ба таври худкор ба кор дароварда шуд"

#: core/ExternalAppModule.cpp:28
#, kde-format
msgid "Relaunch %1"
msgstr "Аз нав ба кор даровардани %1"

#. i18n: ectx: property (windowTitle), widget (QWidget, ExternalModule)
#: core/externalModule.ui:14
#, kde-format
msgid "Dialog"
msgstr "Равзанаи гуфтугӯ"

#: core/ModuleView.cpp:179
#, kde-format
msgid "Reset all current changes to previous values"
msgstr "Бозсозӣ кардани ҳамаи тағйироти ҷорӣ ба қиматҳои пешин"

#: core/ModuleView.cpp:325
#, kde-format
msgid ""
"The settings of the current module have changed.\n"
"Do you want to apply the changes or discard them?"
msgstr ""
"Танзимоти модули ҷорӣ тағйир ёфт.\n"
"Шумо мехоҳед, ки тағйиротро татбиқ ё рад кунед?"

#: core/ModuleView.cpp:327
#, kde-format
msgid "Apply Settings"
msgstr "Татбиқ кардани танзимот"

#: icons/IconMode.cpp:54
#, kde-format
msgid "Icon View"
msgstr "Намуди нишонаҳо"

#: icons/IconMode.cpp:56
#, kde-format
msgid "Provides a categorized icons view of control modules."
msgstr "Модулҳои идоракуниро дар намуди нишонаҳои мураттабшуда нишон медиҳад."

#: icons/IconMode.cpp:59 sidebar/SidebarMode.cpp:180
#: sidebar/SidebarMode.cpp:181
#, kde-format
msgid "Author"
msgstr "Муаллиф"

#: icons/IconMode.cpp:63
#, kde-format
msgid "All Settings"
msgstr "Ҳамаи танзимот"

#: icons/IconMode.cpp:64
#, kde-format
msgid "Keyboard Shortcut: %1"
msgstr "Миёнбурҳои клавиатура: %1"

#: runner/systemsettingsrunner.cpp:40
#, kde-format
msgid "Finds system settings modules whose names or descriptions match :q:"
msgstr ""

#: runner/systemsettingsrunner.cpp:186
#, fuzzy, kde-format
#| msgid "System Settings"
msgid "System Information"
msgstr "Танзимоти низом"

#: sidebar/package/contents/ui/CategoriesPage.qml:58
#, kde-format
msgid "Show intro page"
msgstr "Нишон додани саҳифаи муқаддимавӣ"

#: sidebar/package/contents/ui/CategoriesPage.qml:97
#, kde-format
msgctxt "A search yielded no results"
msgid "No items matching your search"
msgstr "Ягон мавод ба ҷустуҷӯи шумо мувофиқат намекунад"

#: sidebar/package/contents/ui/FooterToolbar.qml:37
#, fuzzy, kde-format
#| msgctxt "Action to show indicators for settings with custom data"
#| msgid "Highlight Changed Settings"
msgctxt ""
"Action to show indicators for settings with custom data. Use as short a "
"translation as is feasible, as horizontal space is limited."
msgid "Highlight Changed Settings"
msgstr "Намоиши танзимоти тағйирёфта"

#: sidebar/package/contents/ui/HamburgerMenuButton.qml:17
#, kde-format
msgid "Show menu"
msgstr "Нишон додани феҳрист"

#: sidebar/package/contents/ui/introPage.qml:56
#, kde-format
msgid "Plasma"
msgstr "Плазма"

#: sidebar/SidebarMode.cpp:175
#, kde-format
msgid "Sidebar View"
msgstr "Намуди навори ҷонибӣ"

#: sidebar/SidebarMode.cpp:177
#, kde-format
msgid "Provides a categorized sidebar for control modules."
msgstr ""
"Модулҳои идоракуниро дар намуди навори ҷонибии мураттабшуда нишон медиҳад."

#: sidebar/SidebarMode.cpp:179
#, kde-format
msgid "(c) 2017, Marco Martin"
msgstr "(c) 2017, Марко Мартин (Marco Martin)"

#: sidebar/SidebarMode.cpp:180
#, kde-format
msgid "Marco Martin"
msgstr "Марко Мартин (Marco Martin)"

#: sidebar/SidebarMode.cpp:669
#, kde-format
msgid "Sidebar"
msgstr "Навори ҷонибӣ"

#: sidebar/SidebarMode.cpp:741
#, kde-format
msgid "Most Used"
msgstr "Истифодашавандаи зиёдтарин"

#: sidebar/ToolTips/tooltipmanager.cpp:237
#, kde-format
msgid "<i>Contains 1 item</i>"
msgid_plural "<i>Contains %1 items</i>"
msgstr[0] "<i>Дорои 1 мавод мебошад</i>"
msgstr[1] "<i>Дорои %1 мавод мебошад</i>"

#~ msgid "Most used module number %1"
#~ msgstr "Рақами модули истифодашавандаи зиёдтарин %1"

#~ msgid "Frequently Used"
#~ msgstr "Бисёр истифодашуда"

#~ msgid "Go back"
#~ msgstr "Бозгашт"

#~ msgid "View Style"
#~ msgstr "Услуби намоиш"

#~ msgid "Show detailed tooltips"
#~ msgstr "Нишон додани маслиҳатҳои ботафсил"

#, fuzzy
#~| msgid "Configure..."
#~ msgid "Configure…"
#~ msgstr "Танзимот..."

#~ msgctxt "General config for System Settings"
#~ msgid "General"
#~ msgstr "Умумӣ"

#~ msgid ""
#~ "System Settings was unable to find any views, and hence nothing is "
#~ "available to configure."
#~ msgstr ""
#~ "Барномаи танзимоти низом ягон намудро ёфт карда натавонист, бинобар ин "
#~ "ягон чиз барои танзимкунӣ дастрас нест."

#~ msgid "Determines whether detailed tooltips should be used"
#~ msgstr "Ниёзи намоиши маслиҳатҳои ботафсил муайян мекунад"

#~ msgctxt "Action to show indicators for settings with custom data"
#~ msgid "Highlight Changed Settings"
#~ msgstr "Намоиши танзимоти тағйирёфта"

#~ msgid "About Active Module"
#~ msgstr "Дар бораи модули фаъол"

#~ msgid "Back"
#~ msgstr "Ба қафо"

#~ msgid "Configure your system"
#~ msgstr "Низоми худро танзим намоед"

#~ msgid ""
#~ "Welcome to \"System Settings\", a central place to configure your "
#~ "computer system."
#~ msgstr ""
#~ "Хуш омадед ба \"Танзимоти низом\", ҷойи марказиест, ки метавонед дар он "
#~ "ҷо низоми компютерии худро танзим намоед."

#~ msgid "Tree View"
#~ msgstr "Намуди дарахтшакл"

#~ msgid "Provides a classic tree-based view of control modules."
#~ msgstr ""
#~ "Модулҳои идоракуниро дар намуди дарахтшакли классикӣ намоиш медиҳад."

#~ msgid "Expand the first level automatically"
#~ msgstr "Дараҷаи аввалро ба таври худкор мекушояд"

#~ msgid "Search..."
#~ msgstr "Ҷустуҷӯ..."

#, fuzzy
#~| msgid "System Settings"
#~ msgid "System Settings Handbook"
#~ msgstr "Танзимотҳои система"

#, fuzzy
#~| msgid "About %1"
#~ msgid "About KDE"
#~ msgstr "Дар бораи %1"

#~ msgid "Overview"
#~ msgstr "Пешнамоиш"

#~ msgid "(c) 2005, Benjamin C. Meyer; (c) 2007, Canonical Ltd"
#~ msgstr "(c) 2005, Benjamin C. Meyer; (c) 2007, Canonical Ltd"

#~ msgid "Benjamin C. Meyer"
#~ msgstr "Benjamin C. Meyer"

#~ msgid "Jonathan Riddell"
#~ msgstr "Jonathan Riddell"

#~ msgid "Contributor"
#~ msgstr "Ҳамкорӣ"

#~ msgid "Michael D. Stemle"
#~ msgstr "Michael D. Stemle"

#~ msgid "Simon Edwards"
#~ msgstr "Simon Edwards"

#~ msgid "Ellen Reitmayr"
#~ msgstr "Ellen Reitmayr"

#~ msgid "Usability"
#~ msgstr "Истифодабарӣ"

#~ msgid "Unsaved Changes"
#~ msgstr "Тағйиротҳои захиранашуда"
